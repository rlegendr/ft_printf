/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   conv_csp.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/10 14:00:23 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/23 10:41:31 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_printf.h"

char	*op_char(va_list ap, t_printf *d)
{
	char		c;
	char		*ans;

	if (d->bad_conv == -1)
		c = va_arg(ap, int);
	else
		c = d->bad_conv;
	if (c == 0)
	{
		d->nb_zero = 1;
		if (d->bad_conv == -1)
			d->ret += 1;
		return (ft_strnew(0));
	}
	if (!(ans = malloc(sizeof(char) * 2)))
		return (error("malloc op char failed", d));
	ans[0] = c;
	ans[1] = '\0';
	return (ans);
}

char	*op_string(va_list ap, t_printf *d)
{
	char		*str;
	char		*ans;
	int			i;

	i = -1;
	str = va_arg(ap, char*);
	if (str == NULL && d->point == 0)
		return (ft_strdup("(null)"));
	if (d->accu == 0 && d->point == 1)
		return (ft_strnew(0));
	if (d->accu == 0)
	{
		if (!(ans = (char*)malloc(sizeof(char) * (ft_strlen(str) + 1))))
			return (error("malloc op string failed", d));
		ans = ft_strcpy(ans, str);
	}
	else
	{
		if (!(ans = (char*)malloc(sizeof(char) * (d->accu + 1))))
			return (error("malloc op string failed", d));
		while (++i < d->accu)
			ans[i] = str[i];
		ans[i] = '\0';
	}
	return (ans);
}

char	*op_pointer(va_list ap, t_printf *d)
{
	void		*str;
	long long	k;
	char		*ans;

	str = va_arg(ap, void*);
	k = (long long)str;
	ans = ft_itoa_printf(k, 16, d);
	return (ans);
}

char	*op_percent(t_printf *d)
{
	char *ans;

	if (!(ans = malloc(sizeof(char) * 2)))
		error("malloc percent failed", d);
	ans[0] = '%';
	ans[1] = '\0';
	return (ans);
}
