/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   conv_arobas.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/21 09:36:23 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/22 12:47:44 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_printf.h"

char	*op_arobas(va_list ap, t_printf *d)
{
	char	*ans;
	int		nb;
	int		isneg;

	nb = 0;
	isneg = 0;
	if (d->h == 1)
	{
		nb = va_arg(ap, int);
		if (nb < 0)
		{
			nb = -nb;
			isneg = 1;
		}
		nb = count_digit(nb) + isneg;
		return (ft_itoa_printf(nb, 10, d));
	}
	if (d->l == 1)
		return (ft_itoa_printf(d->ret, 10, d));
	ans = va_arg(ap, char*);
	if (ans == NULL)
		return (ft_strdup("-1"));
	return (ft_itoa_printf(ft_strlen(ans), 10, d));
}
