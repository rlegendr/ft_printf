/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   conv_f.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rlegendr <rlegendr@student.le-101.fr>      +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/11 14:21:58 by rlegendr     #+#   ##    ##    #+#       */
/*   Updated: 2019/01/23 09:26:07 by rlegendr    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_printf.h"

char	*suppress_useless_char(char *ans, t_printf *d)
{
	int		i;
	int		j;

	i = 0;
	while (ans[i] != '.')
		i++;
	j = i;
	while (i - j < d->accu)
		i++;
	ans[i + 1] = '\0';
	if (ans[i] == '.')
		ans[i] = '\0';
	if (d->no_accu == 1)
		d->accu = 0;
	return (ans);
}

char	*rounded(char *ans, t_printf *d, int i, int ret)
{
	while (ans[i + 1])
		i++;
	if (ans[i] >= '5')
		ret = 1;
	i--;
	while (ret == 1)
	{
		if (ans[i] < '9' && ans[i] != '.')
		{
			ans[i] += 1;
			ret = 0;
		}
		else if (ans[i] == '9')
			ans[i] = '0';
		if (ans[0] == '0')
		{
			ans = add_first_char(ans, '1', d);
			ret = 0;
		}
		i--;
	}
	ans = suppress_useless_char(ans, d);
	return (ans);
}

char	*op_putfloat(long double n, t_printf *d, int i, char *ans)
{
	char			*mantisse;
	char			*tmp;
	char			*tmp2;
	char			*itoa;

	ans = ft_itoa_printf_u((unsigned long long)n, 10, d);
	mantisse = ft_strnew(0);
	tmp = mantisse;
	while (++i <= d->accu + 1)
	{
		n -= (unsigned long long)n;
		n *= 10;
		itoa = ft_itoa_printf_u((unsigned long long)n, 10, d);
		tmp2 = itoa;
		mantisse = ft_strjoin(mantisse, itoa);
		free_strings(tmp, tmp2, NULL, NULL);
		tmp = mantisse;
	}
	mantisse = add_first_char(mantisse, '.', d);
	tmp = ans;
	tmp2 = mantisse;
	ans = ft_strjoin(ans, mantisse);
	ans = rounded(ans, d, 0, 0);
	free_strings(tmp, tmp2, NULL, NULL);
	return (ans);
}

char	*op_float(va_list ap, t_printf *d)
{
	char			*ans;
	long double		n;

	ans = NULL;
	if (d->no_accu == 1 && d->point == 0)
		d->accu = 6;
	if (d->lf == 1)
		n = va_arg(ap, long double);
	else
		n = (long double)va_arg(ap, double);
	if ((1.0 / 0.0) == n)
		d->inf = 1000000;
	if ((1.0 / 0.0) == n)
		return (ft_strdup(d->conv == 'f' ? "inf" : "INF"));
	if (n != n)
		d->nan = 1000000;
	if (n != n)
		return (ft_strdup(d->conv == 'f' ? "nan" : "NAN"));
	if (n < 0)
		d->isneg = 1;
	if (n < 0)
		n = -n;
	ans = op_putfloat(n, d, 0, ans);
	return (ans);
}
