# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: rlegendr <marvin@le-101.fr>                +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/10/03 11:51:34 by rlegendr     #+#   ##    ##    #+#        #
#    Updated: 2019/01/23 10:47:53 by rlegendr    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

NAME    = libftprintf.a
PROJECT_NAME = libftprintf
EXE = ft_libftprintf

FLAGS   = -Wall -Werror -Wextra
CFLAGS  = $(FLAGS) -I. -c

C_LIBFT = libft/ft_abs.c libft/ft_atoi.c libft/ft_brainfuck.c libft/ft_bzero.c libft/ft_factorial.c libft/ft_isalnum.c libft/ft_isalpha.c libft/ft_isascii.c libft/ft_isblank.c libft/ft_iscntrl.c libft/ft_isdigit.c libft/ft_isneg.c libft/ft_isprime.c libft/ft_isprint.c libft/ft_isspace.c libft/ft_itoa.c libft/ft_itoa_base.c libft/ft_lst_totaldel.c libft/ft_lstadd.c libft/ft_lstbck.c libft/ft_lstdel.c libft/ft_lstdelone.c libft/ft_lstiter.c libft/ft_lstlen.c libft/ft_lstmap.c libft/ft_lstnew.c libft/ft_memalloc.c libft/ft_memccpy.c libft/ft_memchr.c libft/ft_memcmp.c libft/ft_memcpy.c libft/ft_memdel.c libft/ft_memmove.c libft/ft_memset.c libft/ft_power.c libft/ft_print_t_list.c libft/ft_print_tab.c libft/ft_putchar.c libft/ft_putchar_fd.c libft/ft_putendl.c libft/ft_putendl_fd.c libft/ft_putnbr.c libft/ft_putnbr_fd.c libft/ft_putstr.c libft/ft_putstr_fd.c libft/ft_range.c libft/ft_split_whitespaces.c libft/ft_sqrt.c libft/ft_strcat.c libft/ft_strchr.c libft/ft_strclr.c libft/ft_strcmp.c libft/ft_strcpy.c libft/ft_strdel.c libft/ft_strdup.c libft/ft_strequ.c libft/ft_striter.c libft/ft_striteri.c libft/ft_strjoin.c libft/ft_strlcat.c libft/ft_strlen.c libft/ft_strmap.c libft/ft_strmapi.c libft/ft_strncat.c libft/ft_strncmp.c libft/ft_strncpy.c libft/ft_strnequ.c libft/ft_strnew.c libft/ft_strnstr.c libft/ft_strrchr.c libft/ft_strrev.c libft/ft_strsplit.c libft/ft_strstr.c libft/ft_strsub.c libft/ft_strtrim.c libft/ft_tabdel.c libft/ft_tolower.c libft/ft_toupper.c libft/get_next_line.c
C_PRINTF = ft_printf/accuracy_size.c \
		   ft_printf/check_error.c \
		   ft_printf/conv_csp_percent.c \
		   ft_printf/conv_dio.c \
		   ft_printf/conv_uxx.c \
		   ft_printf/conv_b_tab.c \
		   ft_printf/conv_arobas.c \
		   ft_printf/flag_hash.c \
		   ft_printf/flag_plus_space.c \
		   ft_printf/flag_zero_minus_space.c \
		   ft_printf/ft_printf.c \
		   ft_printf/conv_f.c \
		   ft_printf/get_params.c \
		   ft_printf/initialisation.c \
		   ft_printf/itoa_base_ft_printf.c \
		   ft_printf/tools.c \
		   ft_printf/tools2.c

HEADERS = libft.h \
		  ft_printf.h

O_LIBFT  = $(C_LIBFT:.c=.o)
O_PRINTF = $(C_PRINTF:.c=.o)
DIR_O    = objects

CC = @gcc

all: $(NAME)

$(NAME): $(O_LIBFT) $(O_PRINTF) $(HEADERS)
	@ar rcs $(NAME) $(O_LIBFT) $(O_PRINTF)
	@ranlib $(NAME)
	@echo "\033[1;36m$(PROJECT_NAME)		\033[1;32m[✓]\033[0;37m"

(DIR_O)/%.o: %.c
	$(CC) $(CFLAGS) $<

exe : $(NAME)
	@gcc -o $(EXE) -I. main.c $(NAME)

clean:
	@rm -rf $(O_LIBFT) $(O_PRINTF)
	@rm -rf *.gch
	@rm -rf $(EXE).dSYM
	@rm -f .DS_Store
	@echo "\033[1;33mclean 	\033[1;36m$(PROJECT_NAME)	\033[1;32m[✓]\033[0;37m"

fclean: clean
	@rm -rf $(NAME)
	@rm -rf $(EXE)
	@rm -rf a.out
	@echo "\033[1;33mfclean 	\033[1;36m$(PROJECT_NAME)	\033[1;32m[✓]\033[0;37m"

re: fclean all
